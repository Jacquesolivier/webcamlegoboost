#!/bin/bash

# This script is used to add all the requirement files and applications for this project
# 

# Uprading the system
sudo apt-get update
sudo apt-get -y upgrade

# Add the app to map gamepad keys to keyboard
# Done directly with a python library ()
# Dans la config du Raspberry activer dans les options avancées glamor graphic
sudo raspi-config

# Add the python files for the Pan-Tilt Hat:
# https://github.com/pimoroni/pantilt-hat
curl https://get.pimoroni.com/pantilthat | bash

# Activate the I2C interface with raspi-config

# Test with Pimoroni/pantilthat/examples
# for example python3 neopixel.python
# Installation of the joystick to keyboard mapper
sudo apt-get install anitimicrox

# Clone de ce dépôt git en local
git clone https://gitlab.com/Jacquesolivier/webcamlegoboost.git
cd webcamlegoboost
# Pour savoir si on est synchro à jour en local au niveau commit
Git status
# Envoie des modifs locales vers le distant :
git push origin master
 # Mettre à jour du dépôt distant vers le local
git pull

# Tests des périphériques bluetooth :
# Démarrage :
systemctl status bluetooth
# Liste des périphs:
hcitool scan

# Allumer la Moga Pro 2 sur B
# Faire la connection graphiquement
