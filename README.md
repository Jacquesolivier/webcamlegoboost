## A webcam with Pan-Tilt on a Lego Boost robot driven by a joystick

Materials :

A lego boost robot:
https://fr.wikipedia.org/wiki/Lego_Boost

A Raspberry PI 3B+
A Pan-Tilt Hat from Pimoroni:
https://shop.pimoroni.com/products/pan-tilt-hat?variant=22408353287

A webcam for Raspberry PI

A led Hat

And https://github.com/AntiMicroX/antimicrox/ for mapping devices (joystick or gamepad) to mouse/keyboard



