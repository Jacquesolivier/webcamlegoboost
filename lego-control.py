# This python script is used to control the motors of the Lego boost with a device
# In this example we use a Moga Pro 2 joystick

#import evdev
from evdev import InputDevice, categorize, ecodes
import evdev
import threading

# for the lego boost:
from pylgbst.hub import MoveHub

# Creates object 'gamepad' to store the data
# you can call it whatever you like
# We list all the devices and take the first as command device

gamepad = InputDevice(evdev.InputDevice(evdev.list_devices()[0]))
"""
jb@computer:~/Compil/lego-boost$ bt-device -l
Added devices:
Moga Pro 2 HID (00:1E:B5:8F:05:E1)
LEGO Move Hub (00:16:53:BA:01:BD)


BOUTONS :
ABS_GAS : DROITE
ABS_BRAKE  : GAUCHE
Y : 589829 : avance
A  : 589825 : recule
"""



def avance():
    global direction, vitesse, hub, tourne, vitesse_droite, vitesse_gauche
    print("avance", direction * vitesse_droite,direction * vitesse_gauche )

    hub.motor_AB.start_speed(direction * vitesse_droite,direction * vitesse_gauche )
    #else :
        #print("heho", direction * vitesse_droite)

        #hub.motor_A.start_speed(direction * vitesse_droite)
        #hub.motor_B.start_speed(direction * vitesse_gauche)
    return

def tourne():
    global direction, vitesse, hub, tourne, vitesse_droite, vitesse_gauche
    print("avance", direction * vitesse_droite,direction * vitesse_gauche )

    hub.motor_A.start_power(direction * vitesse_droite)
    hub.motor_B.start_power(direction * vitesse_gauche)
    return

def stop():
    global direction, vitesse, hub
    try :
        hub.motor_AB.stop()
    except : pass
    return



#prints out device info at start
print(gamepad)

import time

hub = MoveHub()

def cree_thread():
    global dict_thread
    dict_thread = {}
    dict_thread['avance'] = threading.Thread(target=avance)
    dict_thread['tourne'] = threading.Thread(target=tourne)
    dict_thread['stop'] = threading.Thread(target=stop)


cree_thread()
cmd_instant = ''
cmd = ''
direction = 0
code_bouton = 0
tourne =True

vitesse = 0.3 #vitesse du moteur  : jusqu'à 1.
vitesse_droite = vitesse
vitesse_gauche = vitesse
#evdev takes care of polling the controller in a loop
while True :
    event = gamepad.read_one()
    if event is not None :
        #print(1, str(categorize(event)).split(',')[-1])
        #print(2, str(event).split()[-1])
        cmd_instant = str(categorize(event)).split(',')[-1].strip()
        if cmd_instant != "SYN_REPORT" : #évènement de base
            if 'val' in cmd_instant : #il y a un bouton appuyé -> "val 589829"
                code_bouton = int(cmd_instant.split()[-1].strip())
                vitesse_droite = vitesse
                vitesse_gauche = vitesse
            cmd = cmd_instant
            val = int(str(event).split()[-1].strip())
            if code_bouton > 500000 :
                if "ABS_GAS" in cmd_instant :
                    print("droite")
                    val_joy = float(str(event).split()[-1])/256
                    if direction < 0  :
                        vitesse_droite = vitesse + val_joy*0.3
                    else : vitesse_gauche = vitesse + val_joy*0.3
                    tourne =True
                elif "ABS_BRAKE" in cmd_instant :
                    print("gauche")
                    val_joy = float(str(event).split()[-1])/256
                    if direction < 0  :
                        vitesse_gauche = vitesse + val_joy*0.3
                    else :
                        vitesse_droite = vitesse + val_joy*0.3
                    tourne =True

        if cmd == 'hold':
            ####avance/recule##############
            if code_bouton == 589825 :
                direction = 1
            elif code_bouton == 589829 :
                direction = -1
            else :
                direction = 0
            print('GO', val, cmd, code_bouton, direction)
            if cmd_instant != 'hold' and code_bouton!=0 : #up ou down
                try :
                    dict_thread['avance'].start()
                except : pass
                if tourne :
                    print('tourne')
                    #try :
                        #dict_thread['stop'].start()
                    #except  : pass
                    code_bouton = 0
                    #cree_thread()
                    #time.sleep(0.001)
                    #dict_thread['avance'].start()
                    dict_thread['tourne'].start()
                    tourne=False


        elif cmd == "up" :
            print('stop')
            tourne =False
            try :
                dict_thread['stop'].start()
            except  : pass
            code_bouton = 0
            cree_thread()

